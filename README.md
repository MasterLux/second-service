Сервис приема платежа:
Получает запрос.
Проверяет подпись.
Парсит пакет данных и добавляет данные в очередь обработки.
Высчитывает сумму с коммиссией.
Сохраняет в локальную базу в формате:
{id: идентификатор транзакции, user_id: идентификатор клиента, sum: сумма с учетом коммиссии}
и добавляет запись в таблицу user_wallet
