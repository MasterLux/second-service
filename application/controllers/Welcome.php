<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		if (isset($_POST['action']))
		{
			$this->load->model('First'); $this->First->home();
		}
		else
		{
			show_404();
			return;
		}
	}
}
